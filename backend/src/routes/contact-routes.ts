import { Router, Request, Response } from 'express';
import validateContact from '../middlewares/contact-validator';
import contactService from '../services/contact-service';

const router = Router();

const saveContact = async (req: Request, res: Response) => {
  try {
    await contactService.saveContact(req.body);
    return res.status(200).send();
  } catch (err) {
    return res.status(500).send({
      message: err
    });
  }
};

router.post('/', validateContact, saveContact);

export default router;
