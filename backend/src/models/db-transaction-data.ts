type DbTransactionData = string | number | null;

export default DbTransactionData;
