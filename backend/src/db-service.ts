import sqlite3, { Database } from 'sqlite3';
import { DATABASE_NAME } from './config';
import DbTransactionData from './models/db-transaction-data';
import DbTransactionResult from './models/db-transaction-result';

const init = (): void => {
  const dbConnection = openConnection();

  dbConnection.run(`
    CREATE TABLE IF NOT EXISTS contacts(
      firstName TEXT,
      lastName TEXT,
      address TEXT,
      phoneNumber TEXT,
      email TEXT)
  `);

  dbConnection.close();
};

const generateSqlValuesByDataTypes = (data: DbTransactionData[]): string => {
  const sqlValues = data.map(val => {
    return (typeof val === 'string') ? `'${val}'` : val;
  }).join(', ');

  return sqlValues;
};

const insertRow = (tableName: string, data: DbTransactionData[]): Promise<DbTransactionResult> => {
  return new Promise<DbTransactionResult>((resolve, reject) => {
    const dbConnection = openConnection();
    const values = generateSqlValuesByDataTypes(data);

    dbConnection.run(`INSERT INTO ${tableName} VALUES(${values})`, err => {
      if (err) {
        console.error(err.message);
        return reject();
      }
      resolve({});
    });

    dbConnection.close();
  });
};

const openConnection = (): Database => {
  return new sqlite3.Database(`./${DATABASE_NAME}`, err => {
    if (err) { console.error(err.message); }
  });
};

export default {
  init,
  insertRow
};
