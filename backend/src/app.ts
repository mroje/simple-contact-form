import express from 'express';
import cors from 'cors';
import bodyParser from 'body-parser';

import { PORT } from './config';
import dbService from './db-service';
import contactRoutes from './routes/contact-routes';

const app: express.Application = express();

app.use(cors());
app.use(bodyParser.json())
app.use('/contacts', contactRoutes);

dbService.init();

app.listen(PORT, () => {
  console.log(`Server listening on port ${PORT}...`);
});
