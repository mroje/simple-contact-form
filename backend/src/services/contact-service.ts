import Contact from '../models/contact';
import dbService from '../db-service';

const saveContact = async (contact: Contact): Promise<void> => {
  const { firstName, lastName, address, phoneNumber, email } = contact;
  const transactionData = [firstName, lastName, address, phoneNumber, email];

  try {
    await dbService.insertRow('contacts', transactionData);

    console.log(`Contact ${firstName} ${lastName} successfully inserted!`);
    return Promise.resolve();
  } catch (err) {
    return Promise.reject(err);
  }
};

export default {
  saveContact
};
