import { Request, Response, NextFunction } from 'express';

const validateContact = (req: Request, res: Response, next: NextFunction) => {
  const { firstName, lastName, address, phoneNumber, email } = req.body;
  
  if (!firstName || !lastName || !address || !phoneNumber || !email) {
    return res.status(400).send();
  }

  next();
};

export default validateContact;
