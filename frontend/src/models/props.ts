import { MouseEvent, ChangeEvent } from 'react';

// Models
import Contact from './contact';

export interface ButtonProps {
  text: string;
  isDisabled: boolean;
  onClick: (event: MouseEvent) => void;
}

export interface ContactFormProps {
  onSubmit: (contact: Contact) => void;
}

export interface FormInputProps {
  name: string;
  type: string;
  value: string | boolean;
  onChange: (_: ChangeEvent<HTMLInputElement>) => void;
  isTouched: boolean;
  invalidRules: string[];
}
