interface Contact {
  firstName: string;
  lastName: string;
  address: string;
  phoneNumber: string;
  email: string;
}

export default Contact;
