interface FormControlValidationRules {
  isRequired?: boolean;
  isEmail?: boolean;
  isPhoneNumber?: boolean;
}

export default FormControlValidationRules;
