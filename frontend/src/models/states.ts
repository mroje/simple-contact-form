import FormControlValidationRules from './form-validation-rules';

export interface ContactFormState {
  isValid: boolean;
  formControls: {
    [name: string]: {
      value: string | boolean;
      type: string;
      isTouched: boolean;
      invalidRules: string[];
      validationRules?: FormControlValidationRules;
    }
  }
}
