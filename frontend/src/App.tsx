import React, { useEffect } from 'react';

// Styles
import './css/components/App.css';

// Models
import Contact from './models/contact';
import ContactForm from './components/ContactForm';

// Services
import ContactService from './services/contact-service';

const App = () => {
  const contactFormRef = React.createRef<ContactForm>();

  const onFormSubmit = async (contact: Contact) => {
    try {
      await ContactService.saveContact(contact);
      contactFormRef.current?.clearForm();

      // In real app I would show some styled toast message to user (also in catch block)
      alert('Contact successfully saved!');
    } catch (err) {
      console.error(err);
      alert('Contact submit failed. Please try again...');
    }
  };

  return (
    <div className="app-container">
      <ContactForm onSubmit={onFormSubmit} ref={contactFormRef} />
    </div>
  );
}

export default App;
