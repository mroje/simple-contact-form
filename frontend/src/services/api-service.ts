import environment from './../environment.json';

const post = async (endpoint: string, body: object): Promise<any> => {
  try {
    const response = await fetch(`${environment.apiUrl}/${endpoint}`, {
      method: 'POST',
      headers: {
        'Accept': 'application/json',
        'Content-Type': 'application/json'
      },
      body: JSON.stringify(body)
    });

    if (response.status >= 400 && response.status < 600) {
      throw new Error('Bad response from server');
    }
    
    return Promise.resolve();
  } catch (err) {
    return Promise.reject(err);
  }
};

const ApiService = {
  post
};

export default ApiService;
