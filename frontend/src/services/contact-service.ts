import ApiService from './api-service';
import Contact from '../models/contact';

const saveContact = async (contact: Contact): Promise<void> => {
  return await ApiService.post('contacts', contact);
};

const ContactService = {
  saveContact
};

export default ContactService;
