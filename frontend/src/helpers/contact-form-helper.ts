export const initContactFormState = () => {
  return {
    isValid: false,

    formControls: {
      firstName: {
        value: '',
        type: 'text',
        isTouched: false,
        invalidRules: ['isRequired'],
        validationRules: {
          isRequired: true
        }
      },
      lastName: {
        value: '',
        type: 'text',
        isTouched: false,
        invalidRules: ['isRequired'],
        validationRules: {
          isRequired: true
        }
      },
      address: {
        value: '',
        type: 'text',
        isTouched: false,
        invalidRules: ['isRequired'],
        validationRules: {
          isRequired: true
        }
      },
      phoneNumber: {
        value: '',
        type: 'text',
        isTouched: false,
        invalidRules: ['isPhoneNumber', 'isRequired'],
        validationRules: {
          isPhoneNumber: true,
          isRequired: true
        }
      },
      email: {
        value: '',
        type: 'text',
        isTouched: false,
        invalidRules: ['isEmail', 'isRequired'],
        validationRules: {
          isEmail: true,
          isRequired: true
        }
      },
      termsAndConditions: {
        value: false,
        type: 'checkbox',
        isTouched: false,
        invalidRules: ['isRequired'],
        validationRules: {
          isRequired: true
        }
      }
    }
  };
};
