import FormValidationRules from '../models/form-validation-rules';

type FormControlValueType = string | boolean;

export const validateFormControl = (value: FormControlValueType, rules: FormValidationRules): string[] => {
  if (!rules) { return []; }
  
  let invalidRules: string[] = [];
  
  Object.keys(rules).forEach(rule => {
    switch (rule) {
      case 'isRequired':
        const isRequiredRuleValid = isRequiredValidator(value);
        if (!isRequiredRuleValid) {
          invalidRules.push('isRequired');
        }
        break;
      case 'isEmail':
        const isEmailRuleValid = isEmailValidator(value as string);
        if (!isEmailRuleValid) {
          invalidRules.push('isEmail');
        }
        break;
      case 'isPhoneNumber':
        const isPhoneNumberRuleValid = isPhoneNumberValidator(value as string);
        if (!isPhoneNumberRuleValid) {
          invalidRules.push('isPhoneNumber');
        }
        break;
    }
  });
  
  return invalidRules;
};

export const getFormControlErrorMessage = (invalidRules: string[]): string | null => {
  if (invalidRules.includes('isRequired')) {
    return 'Required';
  }
  if (invalidRules.includes('isEmail')) {
    return 'Bad format';
  }
  if (invalidRules.includes('isPhoneNumber')) {
    return 'Bad format (Example: +123456789)';
  }

  return null;
};

const isRequiredValidator = (value: FormControlValueType): boolean => {
  if (typeof value === 'string') {
    return value.trim() !== '';	
  } else if (typeof value === 'boolean') {
    return !!value;
  }

  return false;
};

const isEmailValidator = (value: string): boolean => {
  const regex = /^(([^<>()\[\]\\.,;:\s@"]+(\.[^<>()\[\]\\.,;:\s@"]+)*)|(".+"))@((\[[0-9]{1,3}\.[0-9]{1,3}\.[0-9]{1,3}\.[0-9]{1,3}\])|(([a-zA-Z\-0-9]+\.)+[a-zA-Z]{2,}))$/;
  return regex.test(value.toLowerCase());
}

const isPhoneNumberValidator = (value: string): boolean => {
  const regex = /^\+[0-9]+/;
  return regex.test(value);
};
