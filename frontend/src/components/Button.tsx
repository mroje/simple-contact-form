import React from 'react';

import './../css/components/Button.css';
import { ButtonProps } from '../models/props';

const Button = (props: ButtonProps) => {
  return (
    <button
      className="btn"
      onClick={props.onClick}
      disabled={props.isDisabled}>

      {props.text}
      
    </button>
  );
}

export default Button;
