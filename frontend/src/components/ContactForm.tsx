import React, { Component, MouseEvent, ChangeEvent } from 'react';

// Styles
import './../css/components/ContactForm.css';

// Helpers
import { initContactFormState } from '../helpers/contact-form-helper';
import { validateFormControl } from '../helpers/form-validation-helper';

// Components
import Button from './Button';
import FormInput from './FormInput';

// Models
import Contact from '../models/contact';
import { ContactFormProps } from '../models/props';
import { ContactFormState } from '../models/states';

class ContactForm extends Component<ContactFormProps, ContactFormState> {

  constructor(props: ContactFormProps) {
    super(props);

    this.state = initContactFormState();

    this.handleSubmitButtonClick = this.handleSubmitButtonClick.bind(this);
    this.handleFormInputChange = this.handleFormInputChange.bind(this);
  }

  clearForm(): void {
    const clearedFormControlsState = { ...this.state.formControls };
    
    Object
      .keys(clearedFormControlsState)
      .forEach(name => clearedFormControlsState[name].value = '');
    
    this.setState({
      isValid: this.state.isValid,
      formControls: clearedFormControlsState
    });
  }

  handleFormInputChange(event: ChangeEvent<HTMLInputElement>) {
    let { name, checked } = event.target;
    let value: string | boolean = event.target.value;

    const newFormControlsState = { ...this.state.formControls };
    const changedFormControl = { ...newFormControlsState[name] };

    if (changedFormControl.type === 'checkbox') {
      value = checked;
    }

    changedFormControl.value = value;
    changedFormControl.isTouched = true;
    changedFormControl.invalidRules = validateFormControl(value, changedFormControl.validationRules!);

    newFormControlsState[name] = changedFormControl;

    let isFormValid = Object
      .keys(newFormControlsState)
      .every(name => newFormControlsState[name].invalidRules.length === 0);

    this.setState({
      isValid: isFormValid,
      formControls: newFormControlsState
    }); 
  }

  handleSubmitButtonClick(event: MouseEvent) {
    event.preventDefault();

    const {
      firstName,
      lastName,
      address,
      phoneNumber,
      email
    } = this.state.formControls;

    const contact: Contact = {
      firstName: firstName.value as string,
      lastName: lastName.value as string,
      address: address.value as string,
      phoneNumber: phoneNumber.value as string,
      email: email.value as string
    };

    this.props.onSubmit(contact);
  }

  render() {
    return (
      <form className="contact-form">

        <h1 className="contact-form__title">
          Contact Form
        </h1>

        <span className="contact-form__label">
          First name
        </span>
        <FormInput
          name="firstName"
          type={this.state.formControls.firstName.type}
          value={this.state.formControls.firstName.value as string}
          onChange={this.handleFormInputChange}
          isTouched={this.state.formControls.firstName.isTouched}
          invalidRules={this.state.formControls.firstName.invalidRules} />

        <span className="contact-form__label">
          Last name
        </span>
        <FormInput
          name="lastName"
          type={this.state.formControls.lastName.type}
          value={this.state.formControls.lastName.value as string}
          onChange={this.handleFormInputChange}
          isTouched={this.state.formControls.lastName.isTouched}
          invalidRules={this.state.formControls.lastName.invalidRules} />

        <span className="contact-form__label">
          Address
        </span>
        <FormInput
          name="address"
          type={this.state.formControls.address.type}
          value={this.state.formControls.address.value as string}
          onChange={this.handleFormInputChange}
          isTouched={this.state.formControls.address.isTouched}
          invalidRules={this.state.formControls.address.invalidRules} />

        <span className="contact-form__label">
          Phone number
        </span>
        <FormInput
          name="phoneNumber"
          type={this.state.formControls.phoneNumber.type}
          value={this.state.formControls.phoneNumber.value as string}
          onChange={this.handleFormInputChange}
          isTouched={this.state.formControls.phoneNumber.isTouched}
          invalidRules={this.state.formControls.phoneNumber.invalidRules} />

        <span className="contact-form__label">
          Email
        </span>
        <FormInput
          name="email"
          type={this.state.formControls.email.type}
          value={this.state.formControls.email.value as string}
          onChange={this.handleFormInputChange}
          isTouched={this.state.formControls.email.isTouched}
          invalidRules={this.state.formControls.email.invalidRules} />

        <span className="contact-form__label">
          Check to accept Terms & Conditions
        </span>
        <FormInput
          name="termsAndConditions"
          type="checkbox"
          value={this.state.formControls.termsAndConditions.value as boolean}
          onChange={this.handleFormInputChange}
          isTouched={this.state.formControls.termsAndConditions.isTouched}
          invalidRules={this.state.formControls.termsAndConditions.invalidRules} />

        <div className="contact-form__submit-btn">
          <Button
            text="Submit"
            onClick={this.handleSubmitButtonClick}
            isDisabled={!this.state.isValid} />
        </div>
      </form>
    );
  }
}

export default ContactForm;
