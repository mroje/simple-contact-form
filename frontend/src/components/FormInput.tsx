import React from 'react';

// Styles
import './../css/components/FormInput.css';

// Helpers
import { getFormControlErrorMessage } from '../helpers/form-validation-helper';

// Models
import { FormInputProps } from '../models/props';

const FormInput = (props: FormInputProps) => {
  const {
    isTouched,
    invalidRules,
    name,
    type,
    value,
    onChange
  } = props;

  let inputClassName = 'form-input';
  let errorMessage = null;

  if (isTouched && invalidRules.length > 0) {
    inputClassName += ' error';
    errorMessage = getFormControlErrorMessage(invalidRules);
  }

  return (
    <div className="form-input__container">
      <input
        type={type}
        name={name}
        className={inputClassName}
        value={value as string}
        checked={value as boolean}
        onChange={onChange} />
      
      <p className="form-input__error-message">{!!errorMessage && errorMessage}</p>
    </div>
  );
}

export default FormInput;
