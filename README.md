## Simple contact form implemented with React and Node.js
***

### Steps to run the project:

1. Using terminal navigate to `./backend` folder and run following commands to start the server:
	- `npm install`
	- `npm run build`
	- `npm start`

2. Using another terminal session navigate to `./frontend` folder and run following commands:
	- `npm install`
	- `npm start`
